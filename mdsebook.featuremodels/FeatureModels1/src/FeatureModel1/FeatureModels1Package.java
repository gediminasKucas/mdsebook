/**
 */
package FeatureModel1;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see FeatureModel1.FeatureModels1Factory
 * @model kind="package"
 * @generated
 */
public interface FeatureModels1Package extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "FeatureModel1";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.itu.dk/smdp/Quiz30_2/FeatureModel1";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "dk.itu.smdp.quiz30_2.FeatureModel1";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FeatureModels1Package eINSTANCE = FeatureModel1.impl.FeatureModels1PackageImpl.init();

	/**
	 * The meta object id for the '{@link FeatureModel1.impl.NamedElement1Impl <em>Named Element1</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FeatureModel1.impl.NamedElement1Impl
	 * @see FeatureModel1.impl.FeatureModels1PackageImpl#getNamedElement1()
	 * @generated
	 */
	int NAMED_ELEMENT1 = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT1__NAME = 0;

	/**
	 * The number of structural features of the '<em>Named Element1</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT1_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link FeatureModel1.impl.Model1Impl <em>Model1</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FeatureModel1.impl.Model1Impl
	 * @see FeatureModel1.impl.FeatureModels1PackageImpl#getModel1()
	 * @generated
	 */
	int MODEL1 = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL1__NAME = NAMED_ELEMENT1__NAME;

	/**
	 * The feature id for the '<em><b>Root</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL1__ROOT = NAMED_ELEMENT1_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Model1</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL1_FEATURE_COUNT = NAMED_ELEMENT1_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link FeatureModel1.impl.Feature1Impl <em>Feature1</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FeatureModel1.impl.Feature1Impl
	 * @see FeatureModel1.impl.FeatureModels1PackageImpl#getFeature1()
	 * @generated
	 */
	int FEATURE1 = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE1__NAME = NAMED_ELEMENT1__NAME;

	/**
	 * The feature id for the '<em><b>Subfeatures</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE1__SUBFEATURES = NAMED_ELEMENT1_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Groups</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE1__GROUPS = NAMED_ELEMENT1_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Feature1</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE1_FEATURE_COUNT = NAMED_ELEMENT1_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link FeatureModel1.impl.Group1Impl <em>Group1</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FeatureModel1.impl.Group1Impl
	 * @see FeatureModel1.impl.FeatureModels1PackageImpl#getGroup1()
	 * @generated
	 */
	int GROUP1 = 3;

	/**
	 * The feature id for the '<em><b>Members</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP1__MEMBERS = 0;

	/**
	 * The number of structural features of the '<em>Group1</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP1_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link FeatureModel1.impl.OrGroup1Impl <em>Or Group1</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FeatureModel1.impl.OrGroup1Impl
	 * @see FeatureModel1.impl.FeatureModels1PackageImpl#getOrGroup1()
	 * @generated
	 */
	int OR_GROUP1 = 4;

	/**
	 * The feature id for the '<em><b>Members</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_GROUP1__MEMBERS = GROUP1__MEMBERS;

	/**
	 * The number of structural features of the '<em>Or Group1</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_GROUP1_FEATURE_COUNT = GROUP1_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link FeatureModel1.impl.XorGroup1Impl <em>Xor Group1</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FeatureModel1.impl.XorGroup1Impl
	 * @see FeatureModel1.impl.FeatureModels1PackageImpl#getXorGroup1()
	 * @generated
	 */
	int XOR_GROUP1 = 5;

	/**
	 * The feature id for the '<em><b>Members</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XOR_GROUP1__MEMBERS = GROUP1__MEMBERS;

	/**
	 * The number of structural features of the '<em>Xor Group1</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XOR_GROUP1_FEATURE_COUNT = GROUP1_FEATURE_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link FeatureModel1.Model1 <em>Model1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model1</em>'.
	 * @see FeatureModel1.Model1
	 * @generated
	 */
	EClass getModel1();

	/**
	 * Returns the meta object for the containment reference '{@link FeatureModel1.Model1#getRoot <em>Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Root</em>'.
	 * @see FeatureModel1.Model1#getRoot()
	 * @see #getModel1()
	 * @generated
	 */
	EReference getModel1_Root();

	/**
	 * Returns the meta object for class '{@link FeatureModel1.NamedElement1 <em>Named Element1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element1</em>'.
	 * @see FeatureModel1.NamedElement1
	 * @generated
	 */
	EClass getNamedElement1();

	/**
	 * Returns the meta object for the attribute '{@link FeatureModel1.NamedElement1#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see FeatureModel1.NamedElement1#getName()
	 * @see #getNamedElement1()
	 * @generated
	 */
	EAttribute getNamedElement1_Name();

	/**
	 * Returns the meta object for class '{@link FeatureModel1.Feature1 <em>Feature1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature1</em>'.
	 * @see FeatureModel1.Feature1
	 * @generated
	 */
	EClass getFeature1();

	/**
	 * Returns the meta object for the containment reference list '{@link FeatureModel1.Feature1#getSubfeatures <em>Subfeatures</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Subfeatures</em>'.
	 * @see FeatureModel1.Feature1#getSubfeatures()
	 * @see #getFeature1()
	 * @generated
	 */
	EReference getFeature1_Subfeatures();

	/**
	 * Returns the meta object for the containment reference list '{@link FeatureModel1.Feature1#getGroups <em>Groups</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Groups</em>'.
	 * @see FeatureModel1.Feature1#getGroups()
	 * @see #getFeature1()
	 * @generated
	 */
	EReference getFeature1_Groups();

	/**
	 * Returns the meta object for class '{@link FeatureModel1.Group1 <em>Group1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Group1</em>'.
	 * @see FeatureModel1.Group1
	 * @generated
	 */
	EClass getGroup1();

	/**
	 * Returns the meta object for the reference list '{@link FeatureModel1.Group1#getMembers <em>Members</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Members</em>'.
	 * @see FeatureModel1.Group1#getMembers()
	 * @see #getGroup1()
	 * @generated
	 */
	EReference getGroup1_Members();

	/**
	 * Returns the meta object for class '{@link FeatureModel1.OrGroup1 <em>Or Group1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Or Group1</em>'.
	 * @see FeatureModel1.OrGroup1
	 * @generated
	 */
	EClass getOrGroup1();

	/**
	 * Returns the meta object for class '{@link FeatureModel1.XorGroup1 <em>Xor Group1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Xor Group1</em>'.
	 * @see FeatureModel1.XorGroup1
	 * @generated
	 */
	EClass getXorGroup1();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	FeatureModels1Factory getFeatureModels1Factory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link FeatureModel1.impl.Model1Impl <em>Model1</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FeatureModel1.impl.Model1Impl
		 * @see FeatureModel1.impl.FeatureModels1PackageImpl#getModel1()
		 * @generated
		 */
		EClass MODEL1 = eINSTANCE.getModel1();

		/**
		 * The meta object literal for the '<em><b>Root</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL1__ROOT = eINSTANCE.getModel1_Root();

		/**
		 * The meta object literal for the '{@link FeatureModel1.impl.NamedElement1Impl <em>Named Element1</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FeatureModel1.impl.NamedElement1Impl
		 * @see FeatureModel1.impl.FeatureModels1PackageImpl#getNamedElement1()
		 * @generated
		 */
		EClass NAMED_ELEMENT1 = eINSTANCE.getNamedElement1();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT1__NAME = eINSTANCE.getNamedElement1_Name();

		/**
		 * The meta object literal for the '{@link FeatureModel1.impl.Feature1Impl <em>Feature1</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FeatureModel1.impl.Feature1Impl
		 * @see FeatureModel1.impl.FeatureModels1PackageImpl#getFeature1()
		 * @generated
		 */
		EClass FEATURE1 = eINSTANCE.getFeature1();

		/**
		 * The meta object literal for the '<em><b>Subfeatures</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE1__SUBFEATURES = eINSTANCE.getFeature1_Subfeatures();

		/**
		 * The meta object literal for the '<em><b>Groups</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE1__GROUPS = eINSTANCE.getFeature1_Groups();

		/**
		 * The meta object literal for the '{@link FeatureModel1.impl.Group1Impl <em>Group1</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FeatureModel1.impl.Group1Impl
		 * @see FeatureModel1.impl.FeatureModels1PackageImpl#getGroup1()
		 * @generated
		 */
		EClass GROUP1 = eINSTANCE.getGroup1();

		/**
		 * The meta object literal for the '<em><b>Members</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GROUP1__MEMBERS = eINSTANCE.getGroup1_Members();

		/**
		 * The meta object literal for the '{@link FeatureModel1.impl.OrGroup1Impl <em>Or Group1</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FeatureModel1.impl.OrGroup1Impl
		 * @see FeatureModel1.impl.FeatureModels1PackageImpl#getOrGroup1()
		 * @generated
		 */
		EClass OR_GROUP1 = eINSTANCE.getOrGroup1();

		/**
		 * The meta object literal for the '{@link FeatureModel1.impl.XorGroup1Impl <em>Xor Group1</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FeatureModel1.impl.XorGroup1Impl
		 * @see FeatureModel1.impl.FeatureModels1PackageImpl#getXorGroup1()
		 * @generated
		 */
		EClass XOR_GROUP1 = eINSTANCE.getXorGroup1();

	}

} //FeatureModels1Package
