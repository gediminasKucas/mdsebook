/**
 */
package FeatureModel1.util;

import FeatureModel1.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see FeatureModel1.FeatureModels1Package
 * @generated
 */
public class FeatureModels1AdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static FeatureModels1Package modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureModels1AdapterFactory() {
		if (modelPackage == null) {
			modelPackage = FeatureModels1Package.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeatureModels1Switch modelSwitch =
		new FeatureModels1Switch() {
			public Object caseModel1(Model1 object) {
				return createModel1Adapter();
			}
			public Object caseNamedElement1(NamedElement1 object) {
				return createNamedElement1Adapter();
			}
			public Object caseFeature1(Feature1 object) {
				return createFeature1Adapter();
			}
			public Object caseGroup1(Group1 object) {
				return createGroup1Adapter();
			}
			public Object caseOrGroup1(OrGroup1 object) {
				return createOrGroup1Adapter();
			}
			public Object caseXorGroup1(XorGroup1 object) {
				return createXorGroup1Adapter();
			}
			public Object defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	public Adapter createAdapter(Notifier target) {
		return (Adapter)modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link FeatureModel1.Model1 <em>Model1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see FeatureModel1.Model1
	 * @generated
	 */
	public Adapter createModel1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link FeatureModel1.NamedElement1 <em>Named Element1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see FeatureModel1.NamedElement1
	 * @generated
	 */
	public Adapter createNamedElement1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link FeatureModel1.Feature1 <em>Feature1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see FeatureModel1.Feature1
	 * @generated
	 */
	public Adapter createFeature1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link FeatureModel1.Group1 <em>Group1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see FeatureModel1.Group1
	 * @generated
	 */
	public Adapter createGroup1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link FeatureModel1.OrGroup1 <em>Or Group1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see FeatureModel1.OrGroup1
	 * @generated
	 */
	public Adapter createOrGroup1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link FeatureModel1.XorGroup1 <em>Xor Group1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see FeatureModel1.XorGroup1
	 * @generated
	 */
	public Adapter createXorGroup1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //FeatureModels1AdapterFactory
