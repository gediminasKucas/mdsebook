/**
 */
package FeatureModel1.impl;

import FeatureModel1.FeatureModels1Package;
import FeatureModel1.OrGroup1;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Or Group1</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class OrGroup1Impl extends Group1Impl implements OrGroup1 {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OrGroup1Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return FeatureModels1Package.Literals.OR_GROUP1;
	}

} //OrGroup1Impl
