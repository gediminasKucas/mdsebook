/**
 */
package FeatureModel1.impl;

import FeatureModel1.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FeatureModels1FactoryImpl extends EFactoryImpl implements FeatureModels1Factory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static FeatureModels1Factory init() {
		try {
			FeatureModels1Factory theFeatureModels1Factory = (FeatureModels1Factory)EPackage.Registry.INSTANCE.getEFactory(FeatureModels1Package.eNS_URI);
			if (theFeatureModels1Factory != null) {
				return theFeatureModels1Factory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new FeatureModels1FactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureModels1FactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case FeatureModels1Package.MODEL1: return createModel1();
			case FeatureModels1Package.FEATURE1: return createFeature1();
			case FeatureModels1Package.OR_GROUP1: return createOrGroup1();
			case FeatureModels1Package.XOR_GROUP1: return createXorGroup1();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Model1 createModel1() {
		Model1Impl model1 = new Model1Impl();
		return model1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Feature1 createFeature1() {
		Feature1Impl feature1 = new Feature1Impl();
		return feature1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrGroup1 createOrGroup1() {
		OrGroup1Impl orGroup1 = new OrGroup1Impl();
		return orGroup1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XorGroup1 createXorGroup1() {
		XorGroup1Impl xorGroup1 = new XorGroup1Impl();
		return xorGroup1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureModels1Package getFeatureModels1Package() {
		return (FeatureModels1Package)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	public static FeatureModels1Package getPackage() {
		return FeatureModels1Package.eINSTANCE;
	}

} //FeatureModels1FactoryImpl
