/**
 */
package FeatureModel1.impl;

import FeatureModel1.FeatureModels1Package;
import FeatureModel1.XorGroup1;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Xor Group1</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class XorGroup1Impl extends Group1Impl implements XorGroup1 {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XorGroup1Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return FeatureModels1Package.Literals.XOR_GROUP1;
	}

} //XorGroup1Impl
