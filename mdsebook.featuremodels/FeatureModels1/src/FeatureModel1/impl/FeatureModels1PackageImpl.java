/**
 */
package FeatureModel1.impl;

import FeatureModel1.Feature1;
import FeatureModel1.FeatureModels1Factory;
import FeatureModel1.FeatureModels1Package;
import FeatureModel1.Group1;
import FeatureModel1.Model1;
import FeatureModel1.NamedElement1;
import FeatureModel1.OrGroup1;
import FeatureModel1.XorGroup1;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FeatureModels1PackageImpl extends EPackageImpl implements FeatureModels1Package {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass model1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namedElement1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass feature1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass group1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass orGroup1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xorGroup1EClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see FeatureModel1.FeatureModels1Package#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private FeatureModels1PackageImpl() {
		super(eNS_URI, FeatureModels1Factory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link FeatureModels1Package#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static FeatureModels1Package init() {
		if (isInited) return (FeatureModels1Package)EPackage.Registry.INSTANCE.getEPackage(FeatureModels1Package.eNS_URI);

		// Obtain or create and register package
		FeatureModels1PackageImpl theFeatureModels1Package = (FeatureModels1PackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof FeatureModels1PackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new FeatureModels1PackageImpl());

		isInited = true;

		// Create package meta-data objects
		theFeatureModels1Package.createPackageContents();

		// Initialize created meta-data
		theFeatureModels1Package.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theFeatureModels1Package.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(FeatureModels1Package.eNS_URI, theFeatureModels1Package);
		return theFeatureModels1Package;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModel1() {
		return model1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModel1_Root() {
		return (EReference)model1EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNamedElement1() {
		return namedElement1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedElement1_Name() {
		return (EAttribute)namedElement1EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFeature1() {
		return feature1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeature1_Subfeatures() {
		return (EReference)feature1EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeature1_Groups() {
		return (EReference)feature1EClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGroup1() {
		return group1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGroup1_Members() {
		return (EReference)group1EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOrGroup1() {
		return orGroup1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXorGroup1() {
		return xorGroup1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureModels1Factory getFeatureModels1Factory() {
		return (FeatureModels1Factory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		model1EClass = createEClass(MODEL1);
		createEReference(model1EClass, MODEL1__ROOT);

		namedElement1EClass = createEClass(NAMED_ELEMENT1);
		createEAttribute(namedElement1EClass, NAMED_ELEMENT1__NAME);

		feature1EClass = createEClass(FEATURE1);
		createEReference(feature1EClass, FEATURE1__SUBFEATURES);
		createEReference(feature1EClass, FEATURE1__GROUPS);

		group1EClass = createEClass(GROUP1);
		createEReference(group1EClass, GROUP1__MEMBERS);

		orGroup1EClass = createEClass(OR_GROUP1);

		xorGroup1EClass = createEClass(XOR_GROUP1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Add supertypes to classes
		model1EClass.getESuperTypes().add(this.getNamedElement1());
		feature1EClass.getESuperTypes().add(this.getNamedElement1());
		orGroup1EClass.getESuperTypes().add(this.getGroup1());
		xorGroup1EClass.getESuperTypes().add(this.getGroup1());

		// Initialize classes and features; add operations and parameters
		initEClass(model1EClass, Model1.class, "Model1", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getModel1_Root(), this.getFeature1(), null, "root", null, 1, 1, Model1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(namedElement1EClass, NamedElement1.class, "NamedElement1", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNamedElement1_Name(), ecorePackage.getEString(), "name", null, 0, 1, NamedElement1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(feature1EClass, Feature1.class, "Feature1", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFeature1_Subfeatures(), this.getFeature1(), null, "subfeatures", null, 0, -1, Feature1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeature1_Groups(), this.getGroup1(), null, "groups", null, 0, -1, Feature1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(group1EClass, Group1.class, "Group1", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGroup1_Members(), this.getFeature1(), null, "members", null, 2, -1, Group1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(orGroup1EClass, OrGroup1.class, "OrGroup1", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(xorGroup1EClass, XorGroup1.class, "XorGroup1", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //FeatureModels1PackageImpl
