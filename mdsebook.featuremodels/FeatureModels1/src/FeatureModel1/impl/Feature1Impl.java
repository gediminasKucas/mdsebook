/**
 */
package FeatureModel1.impl;

import FeatureModel1.Feature1;
import FeatureModel1.FeatureModels1Package;
import FeatureModel1.Group1;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Feature1</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link FeatureModel1.impl.Feature1Impl#getSubfeatures <em>Subfeatures</em>}</li>
 *   <li>{@link FeatureModel1.impl.Feature1Impl#getGroups <em>Groups</em>}</li>
 * </ul>
 *
 * @generated
 */
public class Feature1Impl extends NamedElement1Impl implements Feature1 {
	/**
	 * The cached value of the '{@link #getSubfeatures() <em>Subfeatures</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubfeatures()
	 * @generated
	 * @ordered
	 */
	protected EList subfeatures;

	/**
	 * The cached value of the '{@link #getGroups() <em>Groups</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroups()
	 * @generated
	 * @ordered
	 */
	protected EList groups;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Feature1Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return FeatureModels1Package.Literals.FEATURE1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getSubfeatures() {
		if (subfeatures == null) {
			subfeatures = new EObjectContainmentEList(Feature1.class, this, FeatureModels1Package.FEATURE1__SUBFEATURES);
		}
		return subfeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getGroups() {
		if (groups == null) {
			groups = new EObjectContainmentEList(Group1.class, this, FeatureModels1Package.FEATURE1__GROUPS);
		}
		return groups;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FeatureModels1Package.FEATURE1__SUBFEATURES:
				return ((InternalEList)getSubfeatures()).basicRemove(otherEnd, msgs);
			case FeatureModels1Package.FEATURE1__GROUPS:
				return ((InternalEList)getGroups()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FeatureModels1Package.FEATURE1__SUBFEATURES:
				return getSubfeatures();
			case FeatureModels1Package.FEATURE1__GROUPS:
				return getGroups();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FeatureModels1Package.FEATURE1__SUBFEATURES:
				getSubfeatures().clear();
				getSubfeatures().addAll((Collection)newValue);
				return;
			case FeatureModels1Package.FEATURE1__GROUPS:
				getGroups().clear();
				getGroups().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case FeatureModels1Package.FEATURE1__SUBFEATURES:
				getSubfeatures().clear();
				return;
			case FeatureModels1Package.FEATURE1__GROUPS:
				getGroups().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FeatureModels1Package.FEATURE1__SUBFEATURES:
				return subfeatures != null && !subfeatures.isEmpty();
			case FeatureModels1Package.FEATURE1__GROUPS:
				return groups != null && !groups.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //Feature1Impl
