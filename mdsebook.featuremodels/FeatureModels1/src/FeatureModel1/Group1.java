/**
 */
package FeatureModel1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Group1</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link FeatureModel1.Group1#getMembers <em>Members</em>}</li>
 * </ul>
 *
 * @see FeatureModel1.FeatureModels1Package#getGroup1()
 * @model abstract="true"
 * @generated
 */
public interface Group1 extends EObject {
	/**
	 * Returns the value of the '<em><b>Members</b></em>' reference list.
	 * The list contents are of type {@link FeatureModel1.Feature1}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Members</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Members</em>' reference list.
	 * @see FeatureModel1.FeatureModels1Package#getGroup1_Members()
	 * @model type="FeatureModel1.Feature1" lower="2"
	 * @generated
	 */
	EList getMembers();

} // Group1
