/**
 */
package FeatureModel1;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Or Group1</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see FeatureModel1.FeatureModels1Package#getOrGroup1()
 * @model
 * @generated
 */
public interface OrGroup1 extends Group1 {
} // OrGroup1
