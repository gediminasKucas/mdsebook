/**
 */
package FeatureModel2;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature2</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link FeatureModel2.Feature2#getSolitarySubfeatures <em>Solitary Subfeatures</em>}</li>
 *   <li>{@link FeatureModel2.Feature2#getGroups <em>Groups</em>}</li>
 * </ul>
 *
 * @see FeatureModel2.FeatureModels2Package#getFeature2()
 * @model
 * @generated
 */
public interface Feature2 extends NamedElement2 {
	/**
	 * Returns the value of the '<em><b>Solitary Subfeatures</b></em>' containment reference list.
	 * The list contents are of type {@link FeatureModel2.Feature2}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Solitary Subfeatures</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Solitary Subfeatures</em>' containment reference list.
	 * @see FeatureModel2.FeatureModels2Package#getFeature2_SolitarySubfeatures()
	 * @model type="FeatureModel2.Feature2" containment="true"
	 * @generated
	 */
	EList getSolitarySubfeatures();

	/**
	 * Returns the value of the '<em><b>Groups</b></em>' containment reference list.
	 * The list contents are of type {@link FeatureModel2.Group2}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Groups</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Groups</em>' containment reference list.
	 * @see FeatureModel2.FeatureModels2Package#getFeature2_Groups()
	 * @model type="FeatureModel2.Group2" containment="true"
	 * @generated
	 */
	EList getGroups();

} // Feature2
