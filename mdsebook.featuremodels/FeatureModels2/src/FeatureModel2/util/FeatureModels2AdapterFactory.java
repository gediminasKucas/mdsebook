/**
 */
package FeatureModel2.util;

import FeatureModel2.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see FeatureModel2.FeatureModels2Package
 * @generated
 */
public class FeatureModels2AdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static FeatureModels2Package modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureModels2AdapterFactory() {
		if (modelPackage == null) {
			modelPackage = FeatureModels2Package.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeatureModels2Switch modelSwitch =
		new FeatureModels2Switch() {
			public Object caseNamedElement2(NamedElement2 object) {
				return createNamedElement2Adapter();
			}
			public Object caseModel2(Model2 object) {
				return createModel2Adapter();
			}
			public Object caseFeature2(Feature2 object) {
				return createFeature2Adapter();
			}
			public Object caseGroup2(Group2 object) {
				return createGroup2Adapter();
			}
			public Object caseOrGroup2(OrGroup2 object) {
				return createOrGroup2Adapter();
			}
			public Object caseXorGroup2(XorGroup2 object) {
				return createXorGroup2Adapter();
			}
			public Object defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	public Adapter createAdapter(Notifier target) {
		return (Adapter)modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link FeatureModel2.NamedElement2 <em>Named Element2</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see FeatureModel2.NamedElement2
	 * @generated
	 */
	public Adapter createNamedElement2Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link FeatureModel2.Model2 <em>Model2</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see FeatureModel2.Model2
	 * @generated
	 */
	public Adapter createModel2Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link FeatureModel2.Feature2 <em>Feature2</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see FeatureModel2.Feature2
	 * @generated
	 */
	public Adapter createFeature2Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link FeatureModel2.Group2 <em>Group2</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see FeatureModel2.Group2
	 * @generated
	 */
	public Adapter createGroup2Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link FeatureModel2.OrGroup2 <em>Or Group2</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see FeatureModel2.OrGroup2
	 * @generated
	 */
	public Adapter createOrGroup2Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link FeatureModel2.XorGroup2 <em>Xor Group2</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see FeatureModel2.XorGroup2
	 * @generated
	 */
	public Adapter createXorGroup2Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //FeatureModels2AdapterFactory
