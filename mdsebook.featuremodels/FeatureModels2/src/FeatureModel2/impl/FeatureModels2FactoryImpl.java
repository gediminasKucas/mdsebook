/**
 */
package FeatureModel2.impl;

import FeatureModel2.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FeatureModels2FactoryImpl extends EFactoryImpl implements FeatureModels2Factory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static FeatureModels2Factory init() {
		try {
			FeatureModels2Factory theFeatureModels2Factory = (FeatureModels2Factory)EPackage.Registry.INSTANCE.getEFactory(FeatureModels2Package.eNS_URI);
			if (theFeatureModels2Factory != null) {
				return theFeatureModels2Factory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new FeatureModels2FactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureModels2FactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case FeatureModels2Package.MODEL2: return createModel2();
			case FeatureModels2Package.FEATURE2: return createFeature2();
			case FeatureModels2Package.OR_GROUP2: return createOrGroup2();
			case FeatureModels2Package.XOR_GROUP2: return createXorGroup2();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Model2 createModel2() {
		Model2Impl model2 = new Model2Impl();
		return model2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Feature2 createFeature2() {
		Feature2Impl feature2 = new Feature2Impl();
		return feature2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrGroup2 createOrGroup2() {
		OrGroup2Impl orGroup2 = new OrGroup2Impl();
		return orGroup2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XorGroup2 createXorGroup2() {
		XorGroup2Impl xorGroup2 = new XorGroup2Impl();
		return xorGroup2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureModels2Package getFeatureModels2Package() {
		return (FeatureModels2Package)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	public static FeatureModels2Package getPackage() {
		return FeatureModels2Package.eINSTANCE;
	}

} //FeatureModels2FactoryImpl
