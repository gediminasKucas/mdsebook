/**
 */
package FeatureModel2.impl;

import FeatureModel2.Feature2;
import FeatureModel2.FeatureModels2Package;
import FeatureModel2.Group2;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Feature2</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link FeatureModel2.impl.Feature2Impl#getSolitarySubfeatures <em>Solitary Subfeatures</em>}</li>
 *   <li>{@link FeatureModel2.impl.Feature2Impl#getGroups <em>Groups</em>}</li>
 * </ul>
 *
 * @generated
 */
public class Feature2Impl extends NamedElement2Impl implements Feature2 {
	/**
	 * The cached value of the '{@link #getSolitarySubfeatures() <em>Solitary Subfeatures</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSolitarySubfeatures()
	 * @generated
	 * @ordered
	 */
	protected EList solitarySubfeatures;

	/**
	 * The cached value of the '{@link #getGroups() <em>Groups</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroups()
	 * @generated
	 * @ordered
	 */
	protected EList groups;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Feature2Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return FeatureModels2Package.Literals.FEATURE2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getSolitarySubfeatures() {
		if (solitarySubfeatures == null) {
			solitarySubfeatures = new EObjectContainmentEList(Feature2.class, this, FeatureModels2Package.FEATURE2__SOLITARY_SUBFEATURES);
		}
		return solitarySubfeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getGroups() {
		if (groups == null) {
			groups = new EObjectContainmentEList(Group2.class, this, FeatureModels2Package.FEATURE2__GROUPS);
		}
		return groups;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FeatureModels2Package.FEATURE2__SOLITARY_SUBFEATURES:
				return ((InternalEList)getSolitarySubfeatures()).basicRemove(otherEnd, msgs);
			case FeatureModels2Package.FEATURE2__GROUPS:
				return ((InternalEList)getGroups()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FeatureModels2Package.FEATURE2__SOLITARY_SUBFEATURES:
				return getSolitarySubfeatures();
			case FeatureModels2Package.FEATURE2__GROUPS:
				return getGroups();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FeatureModels2Package.FEATURE2__SOLITARY_SUBFEATURES:
				getSolitarySubfeatures().clear();
				getSolitarySubfeatures().addAll((Collection)newValue);
				return;
			case FeatureModels2Package.FEATURE2__GROUPS:
				getGroups().clear();
				getGroups().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case FeatureModels2Package.FEATURE2__SOLITARY_SUBFEATURES:
				getSolitarySubfeatures().clear();
				return;
			case FeatureModels2Package.FEATURE2__GROUPS:
				getGroups().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FeatureModels2Package.FEATURE2__SOLITARY_SUBFEATURES:
				return solitarySubfeatures != null && !solitarySubfeatures.isEmpty();
			case FeatureModels2Package.FEATURE2__GROUPS:
				return groups != null && !groups.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //Feature2Impl
