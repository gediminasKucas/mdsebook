/**
 */
package FeatureModel2.impl;

import FeatureModel2.Feature2;
import FeatureModel2.FeatureModels2Factory;
import FeatureModel2.FeatureModels2Package;
import FeatureModel2.Group2;
import FeatureModel2.Model2;
import FeatureModel2.NamedElement2;
import FeatureModel2.OrGroup2;
import FeatureModel2.XorGroup2;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FeatureModels2PackageImpl extends EPackageImpl implements FeatureModels2Package {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namedElement2EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass model2EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass feature2EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass group2EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass orGroup2EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xorGroup2EClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see FeatureModel2.FeatureModels2Package#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private FeatureModels2PackageImpl() {
		super(eNS_URI, FeatureModels2Factory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link FeatureModels2Package#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static FeatureModels2Package init() {
		if (isInited) return (FeatureModels2Package)EPackage.Registry.INSTANCE.getEPackage(FeatureModels2Package.eNS_URI);

		// Obtain or create and register package
		FeatureModels2PackageImpl theFeatureModels2Package = (FeatureModels2PackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof FeatureModels2PackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new FeatureModels2PackageImpl());

		isInited = true;

		// Create package meta-data objects
		theFeatureModels2Package.createPackageContents();

		// Initialize created meta-data
		theFeatureModels2Package.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theFeatureModels2Package.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(FeatureModels2Package.eNS_URI, theFeatureModels2Package);
		return theFeatureModels2Package;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNamedElement2() {
		return namedElement2EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedElement2_Name() {
		return (EAttribute)namedElement2EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModel2() {
		return model2EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModel2_Root() {
		return (EReference)model2EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFeature2() {
		return feature2EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeature2_SolitarySubfeatures() {
		return (EReference)feature2EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeature2_Groups() {
		return (EReference)feature2EClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGroup2() {
		return group2EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGroup2_Members() {
		return (EReference)group2EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOrGroup2() {
		return orGroup2EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXorGroup2() {
		return xorGroup2EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureModels2Factory getFeatureModels2Factory() {
		return (FeatureModels2Factory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		namedElement2EClass = createEClass(NAMED_ELEMENT2);
		createEAttribute(namedElement2EClass, NAMED_ELEMENT2__NAME);

		model2EClass = createEClass(MODEL2);
		createEReference(model2EClass, MODEL2__ROOT);

		feature2EClass = createEClass(FEATURE2);
		createEReference(feature2EClass, FEATURE2__SOLITARY_SUBFEATURES);
		createEReference(feature2EClass, FEATURE2__GROUPS);

		group2EClass = createEClass(GROUP2);
		createEReference(group2EClass, GROUP2__MEMBERS);

		orGroup2EClass = createEClass(OR_GROUP2);

		xorGroup2EClass = createEClass(XOR_GROUP2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Add supertypes to classes
		model2EClass.getESuperTypes().add(this.getNamedElement2());
		feature2EClass.getESuperTypes().add(this.getNamedElement2());
		orGroup2EClass.getESuperTypes().add(this.getGroup2());
		xorGroup2EClass.getESuperTypes().add(this.getGroup2());

		// Initialize classes and features; add operations and parameters
		initEClass(namedElement2EClass, NamedElement2.class, "NamedElement2", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNamedElement2_Name(), ecorePackage.getEString(), "name", null, 0, 1, NamedElement2.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(model2EClass, Model2.class, "Model2", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getModel2_Root(), this.getFeature2(), null, "root", null, 1, 1, Model2.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(feature2EClass, Feature2.class, "Feature2", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFeature2_SolitarySubfeatures(), this.getFeature2(), null, "solitarySubfeatures", null, 0, -1, Feature2.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeature2_Groups(), this.getGroup2(), null, "groups", null, 0, -1, Feature2.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(group2EClass, Group2.class, "Group2", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGroup2_Members(), this.getFeature2(), null, "members", null, 2, -1, Group2.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(orGroup2EClass, OrGroup2.class, "OrGroup2", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(xorGroup2EClass, XorGroup2.class, "XorGroup2", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //FeatureModels2PackageImpl
