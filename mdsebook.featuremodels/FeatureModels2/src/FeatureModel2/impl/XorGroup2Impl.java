/**
 */
package FeatureModel2.impl;

import FeatureModel2.FeatureModels2Package;
import FeatureModel2.XorGroup2;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Xor Group2</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class XorGroup2Impl extends Group2Impl implements XorGroup2 {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XorGroup2Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return FeatureModels2Package.Literals.XOR_GROUP2;
	}

} //XorGroup2Impl
