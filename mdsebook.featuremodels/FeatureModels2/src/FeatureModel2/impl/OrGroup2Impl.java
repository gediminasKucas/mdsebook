/**
 */
package FeatureModel2.impl;

import FeatureModel2.FeatureModels2Package;
import FeatureModel2.OrGroup2;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Or Group2</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class OrGroup2Impl extends Group2Impl implements OrGroup2 {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OrGroup2Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return FeatureModels2Package.Literals.OR_GROUP2;
	}

} //OrGroup2Impl
