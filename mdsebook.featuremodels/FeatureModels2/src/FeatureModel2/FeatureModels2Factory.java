/**
 */
package FeatureModel2;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see FeatureModel2.FeatureModels2Package
 * @generated
 */
public interface FeatureModels2Factory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FeatureModels2Factory eINSTANCE = FeatureModel2.impl.FeatureModels2FactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Model2</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Model2</em>'.
	 * @generated
	 */
	Model2 createModel2();

	/**
	 * Returns a new object of class '<em>Feature2</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Feature2</em>'.
	 * @generated
	 */
	Feature2 createFeature2();

	/**
	 * Returns a new object of class '<em>Or Group2</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Or Group2</em>'.
	 * @generated
	 */
	OrGroup2 createOrGroup2();

	/**
	 * Returns a new object of class '<em>Xor Group2</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Xor Group2</em>'.
	 * @generated
	 */
	XorGroup2 createXorGroup2();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	FeatureModels2Package getFeatureModels2Package();

} //FeatureModels2Factory
