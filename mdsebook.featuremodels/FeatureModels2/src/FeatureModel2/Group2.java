/**
 */
package FeatureModel2;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Group2</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link FeatureModel2.Group2#getMembers <em>Members</em>}</li>
 * </ul>
 *
 * @see FeatureModel2.FeatureModels2Package#getGroup2()
 * @model abstract="true"
 * @generated
 */
public interface Group2 extends EObject {
	/**
	 * Returns the value of the '<em><b>Members</b></em>' containment reference list.
	 * The list contents are of type {@link FeatureModel2.Feature2}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Members</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Members</em>' containment reference list.
	 * @see FeatureModel2.FeatureModels2Package#getGroup2_Members()
	 * @model type="FeatureModel2.Feature2" containment="true" lower="2"
	 * @generated
	 */
	EList getMembers();

} // Group2
