/**
 */
package FeatureModel2;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Xor Group2</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see FeatureModel2.FeatureModels2Package#getXorGroup2()
 * @model
 * @generated
 */
public interface XorGroup2 extends Group2 {
} // XorGroup2
