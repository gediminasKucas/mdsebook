/**
 */
package FeatureModel2;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see FeatureModel2.FeatureModels2Factory
 * @model kind="package"
 * @generated
 */
public interface FeatureModels2Package extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "FeatureModel2";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.itu.dk/smdp/Quiz30_2/FeatureModel2";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "dk.itu.smdp.quiz30_2.FeatureModel2";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FeatureModels2Package eINSTANCE = FeatureModel2.impl.FeatureModels2PackageImpl.init();

	/**
	 * The meta object id for the '{@link FeatureModel2.impl.NamedElement2Impl <em>Named Element2</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FeatureModel2.impl.NamedElement2Impl
	 * @see FeatureModel2.impl.FeatureModels2PackageImpl#getNamedElement2()
	 * @generated
	 */
	int NAMED_ELEMENT2 = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT2__NAME = 0;

	/**
	 * The number of structural features of the '<em>Named Element2</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT2_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link FeatureModel2.impl.Model2Impl <em>Model2</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FeatureModel2.impl.Model2Impl
	 * @see FeatureModel2.impl.FeatureModels2PackageImpl#getModel2()
	 * @generated
	 */
	int MODEL2 = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL2__NAME = NAMED_ELEMENT2__NAME;

	/**
	 * The feature id for the '<em><b>Root</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL2__ROOT = NAMED_ELEMENT2_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Model2</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL2_FEATURE_COUNT = NAMED_ELEMENT2_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link FeatureModel2.impl.Feature2Impl <em>Feature2</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FeatureModel2.impl.Feature2Impl
	 * @see FeatureModel2.impl.FeatureModels2PackageImpl#getFeature2()
	 * @generated
	 */
	int FEATURE2 = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2__NAME = NAMED_ELEMENT2__NAME;

	/**
	 * The feature id for the '<em><b>Solitary Subfeatures</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2__SOLITARY_SUBFEATURES = NAMED_ELEMENT2_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Groups</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2__GROUPS = NAMED_ELEMENT2_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Feature2</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE2_FEATURE_COUNT = NAMED_ELEMENT2_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link FeatureModel2.impl.Group2Impl <em>Group2</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FeatureModel2.impl.Group2Impl
	 * @see FeatureModel2.impl.FeatureModels2PackageImpl#getGroup2()
	 * @generated
	 */
	int GROUP2 = 3;

	/**
	 * The feature id for the '<em><b>Members</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP2__MEMBERS = 0;

	/**
	 * The number of structural features of the '<em>Group2</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GROUP2_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link FeatureModel2.impl.OrGroup2Impl <em>Or Group2</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FeatureModel2.impl.OrGroup2Impl
	 * @see FeatureModel2.impl.FeatureModels2PackageImpl#getOrGroup2()
	 * @generated
	 */
	int OR_GROUP2 = 4;

	/**
	 * The feature id for the '<em><b>Members</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_GROUP2__MEMBERS = GROUP2__MEMBERS;

	/**
	 * The number of structural features of the '<em>Or Group2</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_GROUP2_FEATURE_COUNT = GROUP2_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link FeatureModel2.impl.XorGroup2Impl <em>Xor Group2</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see FeatureModel2.impl.XorGroup2Impl
	 * @see FeatureModel2.impl.FeatureModels2PackageImpl#getXorGroup2()
	 * @generated
	 */
	int XOR_GROUP2 = 5;

	/**
	 * The feature id for the '<em><b>Members</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XOR_GROUP2__MEMBERS = GROUP2__MEMBERS;

	/**
	 * The number of structural features of the '<em>Xor Group2</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XOR_GROUP2_FEATURE_COUNT = GROUP2_FEATURE_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link FeatureModel2.NamedElement2 <em>Named Element2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element2</em>'.
	 * @see FeatureModel2.NamedElement2
	 * @generated
	 */
	EClass getNamedElement2();

	/**
	 * Returns the meta object for the attribute '{@link FeatureModel2.NamedElement2#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see FeatureModel2.NamedElement2#getName()
	 * @see #getNamedElement2()
	 * @generated
	 */
	EAttribute getNamedElement2_Name();

	/**
	 * Returns the meta object for class '{@link FeatureModel2.Model2 <em>Model2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model2</em>'.
	 * @see FeatureModel2.Model2
	 * @generated
	 */
	EClass getModel2();

	/**
	 * Returns the meta object for the containment reference '{@link FeatureModel2.Model2#getRoot <em>Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Root</em>'.
	 * @see FeatureModel2.Model2#getRoot()
	 * @see #getModel2()
	 * @generated
	 */
	EReference getModel2_Root();

	/**
	 * Returns the meta object for class '{@link FeatureModel2.Feature2 <em>Feature2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature2</em>'.
	 * @see FeatureModel2.Feature2
	 * @generated
	 */
	EClass getFeature2();

	/**
	 * Returns the meta object for the containment reference list '{@link FeatureModel2.Feature2#getSolitarySubfeatures <em>Solitary Subfeatures</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Solitary Subfeatures</em>'.
	 * @see FeatureModel2.Feature2#getSolitarySubfeatures()
	 * @see #getFeature2()
	 * @generated
	 */
	EReference getFeature2_SolitarySubfeatures();

	/**
	 * Returns the meta object for the containment reference list '{@link FeatureModel2.Feature2#getGroups <em>Groups</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Groups</em>'.
	 * @see FeatureModel2.Feature2#getGroups()
	 * @see #getFeature2()
	 * @generated
	 */
	EReference getFeature2_Groups();

	/**
	 * Returns the meta object for class '{@link FeatureModel2.Group2 <em>Group2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Group2</em>'.
	 * @see FeatureModel2.Group2
	 * @generated
	 */
	EClass getGroup2();

	/**
	 * Returns the meta object for the containment reference list '{@link FeatureModel2.Group2#getMembers <em>Members</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Members</em>'.
	 * @see FeatureModel2.Group2#getMembers()
	 * @see #getGroup2()
	 * @generated
	 */
	EReference getGroup2_Members();

	/**
	 * Returns the meta object for class '{@link FeatureModel2.OrGroup2 <em>Or Group2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Or Group2</em>'.
	 * @see FeatureModel2.OrGroup2
	 * @generated
	 */
	EClass getOrGroup2();

	/**
	 * Returns the meta object for class '{@link FeatureModel2.XorGroup2 <em>Xor Group2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Xor Group2</em>'.
	 * @see FeatureModel2.XorGroup2
	 * @generated
	 */
	EClass getXorGroup2();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	FeatureModels2Factory getFeatureModels2Factory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link FeatureModel2.impl.NamedElement2Impl <em>Named Element2</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FeatureModel2.impl.NamedElement2Impl
		 * @see FeatureModel2.impl.FeatureModels2PackageImpl#getNamedElement2()
		 * @generated
		 */
		EClass NAMED_ELEMENT2 = eINSTANCE.getNamedElement2();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT2__NAME = eINSTANCE.getNamedElement2_Name();

		/**
		 * The meta object literal for the '{@link FeatureModel2.impl.Model2Impl <em>Model2</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FeatureModel2.impl.Model2Impl
		 * @see FeatureModel2.impl.FeatureModels2PackageImpl#getModel2()
		 * @generated
		 */
		EClass MODEL2 = eINSTANCE.getModel2();

		/**
		 * The meta object literal for the '<em><b>Root</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL2__ROOT = eINSTANCE.getModel2_Root();

		/**
		 * The meta object literal for the '{@link FeatureModel2.impl.Feature2Impl <em>Feature2</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FeatureModel2.impl.Feature2Impl
		 * @see FeatureModel2.impl.FeatureModels2PackageImpl#getFeature2()
		 * @generated
		 */
		EClass FEATURE2 = eINSTANCE.getFeature2();

		/**
		 * The meta object literal for the '<em><b>Solitary Subfeatures</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE2__SOLITARY_SUBFEATURES = eINSTANCE.getFeature2_SolitarySubfeatures();

		/**
		 * The meta object literal for the '<em><b>Groups</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE2__GROUPS = eINSTANCE.getFeature2_Groups();

		/**
		 * The meta object literal for the '{@link FeatureModel2.impl.Group2Impl <em>Group2</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FeatureModel2.impl.Group2Impl
		 * @see FeatureModel2.impl.FeatureModels2PackageImpl#getGroup2()
		 * @generated
		 */
		EClass GROUP2 = eINSTANCE.getGroup2();

		/**
		 * The meta object literal for the '<em><b>Members</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GROUP2__MEMBERS = eINSTANCE.getGroup2_Members();

		/**
		 * The meta object literal for the '{@link FeatureModel2.impl.OrGroup2Impl <em>Or Group2</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FeatureModel2.impl.OrGroup2Impl
		 * @see FeatureModel2.impl.FeatureModels2PackageImpl#getOrGroup2()
		 * @generated
		 */
		EClass OR_GROUP2 = eINSTANCE.getOrGroup2();

		/**
		 * The meta object literal for the '{@link FeatureModel2.impl.XorGroup2Impl <em>Xor Group2</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see FeatureModel2.impl.XorGroup2Impl
		 * @see FeatureModel2.impl.FeatureModels2PackageImpl#getXorGroup2()
		 * @generated
		 */
		EClass XOR_GROUP2 = eINSTANCE.getXorGroup2();

	}

} //FeatureModels2Package
