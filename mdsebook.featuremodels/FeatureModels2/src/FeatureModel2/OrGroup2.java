/**
 */
package FeatureModel2;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Or Group2</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see FeatureModel2.FeatureModels2Package#getOrGroup2()
 * @model
 * @generated
 */
public interface OrGroup2 extends Group2 {
} // OrGroup2
