/**
 */
package FeatureModel2;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model2</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link FeatureModel2.Model2#getRoot <em>Root</em>}</li>
 * </ul>
 *
 * @see FeatureModel2.FeatureModels2Package#getModel2()
 * @model
 * @generated
 */
public interface Model2 extends NamedElement2 {
	/**
	 * Returns the value of the '<em><b>Root</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root</em>' containment reference.
	 * @see #setRoot(Feature2)
	 * @see FeatureModel2.FeatureModels2Package#getModel2_Root()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Feature2 getRoot();

	/**
	 * Sets the value of the '{@link FeatureModel2.Model2#getRoot <em>Root</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Root</em>' containment reference.
	 * @see #getRoot()
	 * @generated
	 */
	void setRoot(Feature2 value);

} // Model2
