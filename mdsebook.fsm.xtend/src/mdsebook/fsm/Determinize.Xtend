// (c) 2014, Andrzej Wąsowski
// WARNING TODO: THIS IS UNFINISHED.
package mdsebook.fsm

import java.util.HashMap
import java.util.HashSet
import java.util.Set

// Turns a nondeterministic FSM into a deterministic one
class Determinize {
	
	val private static HashMap<Set<State>,State> mapping = null 
	
	// collapses a number of states into a new state (presumably states reachable by 
	// a set of nondeterministic transitions). 
	def private static State collapse (Set<State> ss) {
		var State retval = null
		if (!mapping.containsKey(ss)) 
			retval = mapping.put(ss,FsmFactory.eINSTANCE.createState => [
				machine = ss.head.machine
				name = ss.map [name].toString
			])
		else retval = mapping.get (ss)
		
	}
	
	// collapses a number of transitions (presumably with same input)
	// into one transition to a joint state
	def private static collapse (Iterable<Transition> transitions) {
		FsmFactory.eINSTANCE.createTransition => [t|
				t.source = transitions.head.source
				t.input = transitions.head.input
				t.output = transitions.map [t.output].toString

				var target = new HashSet()
				target.addAll (transitions.map [t.target])
				t.target = collapse (target)
			]
	}
	
	// determinizes a state
	def private static determinize(State it) {
		val inputs = leavingTransitions.map [t | t.input]
		val unique_inputs = new HashSet(inputs)
		unique_inputs.map [i | collapse (leavingTransitions.filter [t|t.input == i])]
		
	}

	// assumes that there is no idle transition yet 
	// (otherwise it would introduce nondeterminism)
	def static void addIdle(State s) {
		s.leavingTransitions.add(
			FsmFactory.eINSTANCE.createTransition 
				=> [input = "idle"; output = null; target = s])
	} 

	def static void run(FiniteStateMachine it) {
		states.forEach [ if(!hasIdle) addIdle ]
	}
}