package mdsebook.fsm

// This example is implemented as an in-place transformation
// it adds an idle transition to every state it sees
class AddIdleLoops {

	def static boolean hasIdle(State it) {
		leavingTransitions.exists [input == 'idle']
	}

	// assumes that there is no idle transition yet 
	// (otherwise it would introduce nondeterminism)
	def static void addIdle(State s) {
		s.leavingTransitions.add(
			FsmFactory.eINSTANCE.createTransition 
				=> [input = "idle"; output = null; target = s])
	} 

	def static void run(FiniteStateMachine it) {
		states.forEach [ if(!hasIdle) addIdle ]
	}
}



