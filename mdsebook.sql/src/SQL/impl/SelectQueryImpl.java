/**
 */
package SQL.impl;

import SQL.Column;
import SQL.SelectQuery;
import SQL.SqlPackage;
import SQL.Table;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Select Query</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link SQL.impl.SelectQueryImpl#getWhat <em>What</em>}</li>
 *   <li>{@link SQL.impl.SelectQueryImpl#getFrom <em>From</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SelectQueryImpl extends MinimalEObjectImpl.Container implements SelectQuery {
	/**
	 * The cached value of the '{@link #getWhat() <em>What</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWhat()
	 * @generated
	 * @ordered
	 */
	protected EList what;

	/**
	 * The cached value of the '{@link #getFrom() <em>From</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrom()
	 * @generated
	 * @ordered
	 */
	protected EList from;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SelectQueryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return SqlPackage.Literals.SELECT_QUERY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getWhat() {
		if (what == null) {
			what = new EObjectResolvingEList(Column.class, this, SqlPackage.SELECT_QUERY__WHAT);
		}
		return what;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getFrom() {
		if (from == null) {
			from = new EObjectResolvingEList(Table.class, this, SqlPackage.SELECT_QUERY__FROM);
		}
		return from;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SqlPackage.SELECT_QUERY__WHAT:
				return getWhat();
			case SqlPackage.SELECT_QUERY__FROM:
				return getFrom();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SqlPackage.SELECT_QUERY__WHAT:
				getWhat().clear();
				getWhat().addAll((Collection)newValue);
				return;
			case SqlPackage.SELECT_QUERY__FROM:
				getFrom().clear();
				getFrom().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case SqlPackage.SELECT_QUERY__WHAT:
				getWhat().clear();
				return;
			case SqlPackage.SELECT_QUERY__FROM:
				getFrom().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SqlPackage.SELECT_QUERY__WHAT:
				return what != null && !what.isEmpty();
			case SqlPackage.SELECT_QUERY__FROM:
				return from != null && !from.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SelectQueryImpl
