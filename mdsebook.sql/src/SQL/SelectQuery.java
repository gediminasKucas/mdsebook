/**
 */
package SQL;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Select Query</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link SQL.SelectQuery#getWhat <em>What</em>}</li>
 *   <li>{@link SQL.SelectQuery#getFrom <em>From</em>}</li>
 * </ul>
 *
 * @see SQL.SqlPackage#getSelectQuery()
 * @model
 * @generated
 */
public interface SelectQuery extends EObject {
	/**
	 * Returns the value of the '<em><b>What</b></em>' reference list.
	 * The list contents are of type {@link SQL.Column}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>What</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>What</em>' reference list.
	 * @see SQL.SqlPackage#getSelectQuery_What()
	 * @model type="SQL.Column" required="true"
	 * @generated
	 */
	EList getWhat();

	/**
	 * Returns the value of the '<em><b>From</b></em>' reference list.
	 * The list contents are of type {@link SQL.Table}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From</em>' reference list.
	 * @see SQL.SqlPackage#getSelectQuery_From()
	 * @model type="SQL.Table" required="true"
	 * @generated
	 */
	EList getFrom();

} // SelectQuery
