/**
 */
package SQL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Column</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see SQL.SqlPackage#getColumn()
 * @model
 * @generated
 */
public interface Column extends NamedElement {
} // Column
