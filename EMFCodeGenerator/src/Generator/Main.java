package Generator;

import java.util.Collections;

import org.eclipse.emf.codegen.ecore.generator.*;
import org.eclipse.emf.codegen.ecore.genmodel.GenJDKLevel;
import org.eclipse.emf.codegen.ecore.genmodel.GenModel;
import org.eclipse.emf.codegen.ecore.genmodel.GenModelFactory;
import org.eclipse.emf.codegen.ecore.genmodel.GenModelPackage;
import org.eclipse.emf.codegen.ecore.genmodel.generator.GenBaseGeneratorAdapter;
import org.eclipse.emf.codegen.ecore.genmodel.generator.GenClassGeneratorAdapter;
import org.eclipse.emf.codegen.ecore.genmodel.generator.GenModelGeneratorAdapterFactory;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.util.BasicMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.codegen.ecore.generator.Generator;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.resource.impl.*;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// TODO load genModel. HOW??
		Object genModel = null;
		
//		ResourceSet rs = new ResourceSet();
		Generator g = new Generator();
//		GeneratorAdapterFactory gaf = new AbstractGeneratorAdapterFactory() {
//
//			@Override
//			protected Adapter createAdapter(Notifier arg0) {
//				// TODO Auto-generated method stub
//				return null;
//			}
//
//			@Override
//			public void dispose() {
//				// TODO Auto-generated method stub
//				
//			}
//		};
//		GenBaseGeneratorAdapter gbga = new GenBaseGeneratorAdapter();
//		GenClassGeneratorAdapter gcga = new GenClassGeneratorAdapter(gaf);
		
		   // Globally register the default generator adapter factory for GenModel
		   // elements (only needed in stand-alone).
		   // 
		   GeneratorAdapterFactory.Descriptor.Registry.INSTANCE.addDescriptor
		     (GenModelPackage.eNS_URI, GenModelGeneratorAdapterFactory.DESCRIPTOR);
		 
		   // Create the generator and set the model-level input object.
		   // 
		   Generator generator = new Generator();
		   generator.setInput(genModel);
		 
		   // Generator model code.
		   //
		   generator.generate
		     (genModel, GenBaseGeneratorAdapter.MODEL_PROJECT_TYPE,
		      new BasicMonitor.Printing(System.out));
	}
	
	// DOESN'T WORK
	protected GenModel loadModel(String filename)
	{
	  ResourceSet rs = new ResourceSetImpl();
	  // Problem importing EcoreResourceFactoryImpl, most important
	  rs.getResourceFactoryRegistry().getExtensionToFactoryMap().put("ecore", new EcoreResourceFactoryImpl());

	  //Problem importing TestUtil, AllSuites
//	  URI baseURI = URI.createFileURI(TestUtil.getPluginDirectory(AllSuites.PLUGIN_ID)).appendSegment("data").appendSegment("");
	  URI baseURI = null;
	  URI uri = URI.createFileURI(filename).resolve(baseURI);
	  Resource resource = rs.getResource(uri, true);
//	  assertEquals(1, resource.getContents().size());

	  EPackage ePackage = (EPackage)resource.getContents().get(0);
	  GenModel genModel = GenModelFactory.eINSTANCE.createGenModel();
	  genModel.initialize(Collections.singletonList(ePackage));
	  genModel.setComplianceLevel(GenJDKLevel.JDK50_LITERAL);
//	  assertEquals(1, genModel.getGenPackages().size());

	  return genModel;
	}

}
